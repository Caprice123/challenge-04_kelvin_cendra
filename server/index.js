import http from 'http';
import dotenv from 'dotenv'
import Server from './routes.js'

dotenv.config()

const host = process.env.HOST || 'localhost'
const port = process.env.PORT || 5000

const app = http.createServer((req, res) => {
    const server = new Server(req, res)
    server.init()
})


app.listen(port, host, () => {
    console.log(`Server listen to ${port}`)
})
