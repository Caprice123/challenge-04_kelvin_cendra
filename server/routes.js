import fs from 'fs'
import path from 'path';
const f = fs.promises;

class Server{
    constructor(req, res){
        this.req = req
        this.res = res
    }


    init(){
        const { url } = this.req
        
        if (url === '/'){
            this.getLandingPage()
        } else if (url.includes('/cars') ){
            this.getCars()
        } else if (url === '/api/v1/list-car'){
            this.getJSON()
        } else {
            this.handleStatic(url)
        }
    }
    
    async getLandingPage(){
        try{
            const file = await f.readFile("./public/index.html")
            this.res.writeHead(200)
            this.res.end(file)

        } catch(err){
            console.log(err)
            this.res.writeHead(500)
            this.res.end()
        }
    }

    async getCars(){
        try{
            const file = await f.readFile("./public/rentCar.html")
            this.res.writeHead(200)
            this.res.end(file)

        } catch(err){
            console.log(err)
            this.res.writeHead(500)
            this.res.end()
        }
    }

    getJSON(){
        this.res.writeHead(200, { "Content-type": "application/json" })
        
        var file = fs.createReadStream(`./data/cars.min.json`)
        file.pipe(this.res)
    }

    handleStatic(url){
        const contentType = {
            css: "text/css",
            svg: "image/svg+xml",
            jpg: "image/jpg",
            js: "text/javascript",
            ico: "image/x-icon"
        }
       
        const extension = path.extname(url).replaceAll(".", "")
        this.res.writeHead(200, { "Content-type": contentType[extension] })
        
        var file = fs.createReadStream(`.${url}`)
        file.pipe(this.res)
    }
}

export default Server