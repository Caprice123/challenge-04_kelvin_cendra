class Car {
    static list = [];

    static init(cars) {
        this.list = cars.map((i) => new this(i));
    }

    constructor({
        id,
        plate,
        manufacture,
        model,
        image,
        rentPerDay,
        capacity,
        description,
        transmission,
        available,
        type,
        year,
        options,
        specs,
        availableAt,
        withDriver
    }) {
        this.id = id;
        this.plate = plate;
        this.manufacture = manufacture;
        this.model = model;
        this.image = image;
        this.rentPerDay = rentPerDay;
        this.capacity = capacity;
        this.description = description;
        this.transmission = transmission;
        this.available = available;
        this.type = type;
        this.year = year;
        this.options = options;
        this.specs = specs;
        this.availableAt = availableAt;
        this.withDriver = withDriver
    }

    render() {
        this.image = this.image.replaceAll("./", "./public/")
        return `
            <img src="${this.image}" class="mx-auto w-75 pt-3" alt="${this.manufacture}" />
            <div class="card-body w-75 mx-auto d-flex flex-column justify-content-between">
                <div class="mt-3">
                    <p>${this.model} / ${this.type}</p>
                </div>
                <div class="mt-3">
                    <p><strong>Rp. ${this.rentPerDay.toLocaleString()} / hari </strong></p>
                </div>
                <div class="mt-3" >
                    <p>${this.description}</p>
                </div>
                <div class="mt-3">
                    <img src='./public/images/icon_user.svg' alt='icon user' class='icon-card ' /><span class="ms-2">${this.capacity} orang</span>
                </div>
                <div class="mt-3">
                    <img src='./public/images/icon_setting.svg' alt='icon setting' class='icon-card ' /><span class="ms-2">${this.transmission}</span>
                </div>
                <div class="mt-3">
                    <img src='./public/images/icon_calender.svg' alt='icon setting' class='icon-card ' /><span class="ms-2">${this.year}</span>
                </div>
                <div class="mt-3">
                    <button type="button" class="btn btn-success w-100 bg-limegreen04 no-border select-car" data-id="${this.id}" id="select-car" onclick="App.openPopup('${this.id}')">Pilih Mobil</button>
                </div>
            </div>
            `;
    }
}
