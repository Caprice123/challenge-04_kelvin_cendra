function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Binar {
    static populateCars = (cars) => {
        return cars.map((car) => {
            const isPositive = getRandomInt(0, 1) === 1;
            const timeAt = new Date();
            const mutator = getRandomInt(1000000, 100000000);
            const dayName = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
            const monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
            let availableAt = new Date(timeAt.getTime() + (isPositive ? mutator : -1 * mutator))
            availableAt = `${dayName[availableAt.getDay()]}, ${availableAt.getDate()} ${monthName[availableAt.getMonth()]} ${availableAt.getFullYear()} ${('00' + availableAt.getHours()).slice(-2)}:${('00' + availableAt.getMinutes()).slice(-2)}:${('00' + availableAt.getSeconds()).slice(-2)}`
            const withDriver = Math.round(Math.random() * 1) === 1
            
            return {
              ...car,
              availableAt,
              withDriver
            };
        })
    }

    static async listCars(filterer) {
        let cars;
        let cachedCarsString = localStorage.getItem("CARS");

        if (cachedCarsString) {
            const cacheCars = JSON.parse(cachedCarsString);
            cars = this.populateCars(cacheCars);
        } else {
            // const response = await fetch(
            //   "https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json"
            // );
            const response = await fetch(
              "/api/v1/list-car"
            );
            const body = await response.json();
            cars = this.populateCars(body)

            localStorage.setItem("CARS", JSON.stringify(cars));
        }

        if (filterer instanceof Function) return cars.filter(filterer);

        return cars;
    }
}
